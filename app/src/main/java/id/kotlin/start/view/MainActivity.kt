package id.kotlin.start.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.kotlin.start.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    setTheme(R.style.AppTheme)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setSupportActionBar(toolbar_main)

    btn_save.setOnClickListener {
      startActivity(
        Intent(
          this@MainActivity,
          DetailActivity::class.java
        )
      )
    }
  }
}