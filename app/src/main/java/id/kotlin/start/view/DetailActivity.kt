package id.kotlin.start.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.Tab
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import id.kotlin.start.R
import id.kotlin.start.adapter.DetailAdapter
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    setSupportActionBar(toolbar_detail)
    supportActionBar?.apply {
      setDisplayHomeAsUpEnabled(true)
      setHomeAsUpIndicator(ContextCompat.getDrawable(this@DetailActivity, R.drawable.ic_back_arrow))
    }

    val tabs = listOf(
      tl_detail.newTab().setText(R.string.status),
      tl_detail.newTab().setText(R.string.history),
      tl_detail.newTab().setText(R.string.notification)
    )
    vp_detail.adapter = DetailAdapter(supportFragmentManager, tl_detail, tabs)
    vp_detail.addOnPageChangeListener(TabLayoutOnPageChangeListener(tl_detail))
    tl_detail.addOnTabSelectedListener(object : OnTabSelectedListener {
      override fun onTabSelected(tab: Tab?) {
        tab?.let { vp_detail.currentItem = tab.position }
      }

      override fun onTabUnselected(tab: Tab?) {}
      override fun onTabReselected(tab: Tab?) {}
    })
  }

  override fun onSupportNavigateUp(): Boolean {
    onBackPressed()
    return super.onSupportNavigateUp()
  }
}