package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.model.StatusModel
import kotlinx.android.synthetic.main.view_product.view.*
import kotlinx.android.synthetic.main.view_recommendation.view.*

class StatusAdapter(private val model: StatusModel) : Adapter<ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
    when (viewType) {
      0 -> RecommendationViewHolder(
        LayoutInflater.from(parent.context).inflate(
          R.layout.view_recommendation,
          parent,
          false
        ), model.recommendations
      )
      1 -> ProductViewHolder(
        LayoutInflater.from(parent.context).inflate(
          R.layout.view_product,
          parent,
          false
        ), model.products
      )
      else -> throw Exception("Unknown adapter!")
    }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    when (holder) {
      is RecommendationViewHolder -> holder.bind()
      is ProductViewHolder -> holder.bind()
    }
  }

  override fun getItemCount(): Int = 2

  override fun getItemViewType(position: Int): Int =
    when (position) {
      0 -> 0
      1 -> 1
      else -> throw Exception("Unknown adapter type!")
    }

  inner class RecommendationViewHolder(
    itemView: View,
    private val recommendations: List<String>
  ) : ViewHolder(itemView) {

    fun bind() {
      with(itemView) {
        rv_recommendation.let { view ->
          view.adapter = RecommendationAdapter(recommendations)
          PagerSnapHelper().attachToRecyclerView(view)
        }
      }
    }
  }

  inner class ProductViewHolder(
    itemView: View,
    private val products: List<String>
  ) : ViewHolder(itemView) {

    fun bind() {
      with(itemView) { rv_product.adapter = ProductAdapter(products) }
    }
  }
}