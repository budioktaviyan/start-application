package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.adapter.TodayAdapter.TodayViewHolder
import kotlinx.android.synthetic.main.item_today.view.*

class TodayAdapter(private val contents: List<String>) : Adapter<TodayViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodayViewHolder =
    TodayViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.item_today,
        parent,
        false
      )
    )

  override fun onBindViewHolder(holder: TodayViewHolder, position: Int) {
    holder.bind(contents[holder.adapterPosition])
  }

  override fun getItemCount(): Int = contents.size

  inner class TodayViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bind(content: String) {
      with(itemView) { tv_item_today.text = content }
    }
  }
}