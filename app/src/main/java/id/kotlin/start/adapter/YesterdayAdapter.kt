package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.adapter.YesterdayAdapter.YesterdayViewHolder
import kotlinx.android.synthetic.main.item_yesterday.view.*

class YesterdayAdapter(private val contents: List<String>) : Adapter<YesterdayViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YesterdayViewHolder =
    YesterdayViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.item_yesterday,
        parent,
        false
      )
    )

  override fun onBindViewHolder(holder: YesterdayViewHolder, position: Int) {
    holder.bind(contents[holder.adapterPosition])
  }

  override fun getItemCount(): Int = contents.size

  inner class YesterdayViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bind(content: String) {
      with(itemView) { tv_item_yesterday.text = content }
    }
  }
}