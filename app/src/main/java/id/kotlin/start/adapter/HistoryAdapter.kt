package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.adapter.HistoryAdapter.HistoryViewHolder
import id.kotlin.start.model.HistoryModel
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(private val histories: List<HistoryModel>) : Adapter<HistoryViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder =
    HistoryViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.item_history,
        parent,
        false
      )
    )

  override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
    holder.bind(histories[holder.adapterPosition])
  }

  override fun getItemCount(): Int = histories.size

  inner class HistoryViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bind(model: HistoryModel) {
      with(itemView) {
        tv_history_title.text = model.title
        tv_history_description.text = model.description
      }
    }
  }
}