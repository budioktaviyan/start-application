package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.adapter.ProductAdapter.ProductViewHolder
import kotlinx.android.synthetic.main.item_product.view.*

class ProductAdapter(private val contents: List<String>) : Adapter<ProductViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
    ProductViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.item_product,
        parent,
        false
      )
    )

  override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
    holder.bind(contents[holder.adapterPosition])
  }

  override fun getItemCount(): Int = contents.size

  inner class ProductViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bind(content: String) {
      with(itemView) { tv_item_product.text = content }
    }
  }
}