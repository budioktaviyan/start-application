package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.adapter.RecommendationAdapter.RecommendationViewHolder
import kotlinx.android.synthetic.main.item_recommendation.view.*

class RecommendationAdapter(private val contents: List<String>) : Adapter<RecommendationViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendationViewHolder =
    RecommendationViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.item_recommendation,
        parent,
        false
      )
    )

  override fun onBindViewHolder(holder: RecommendationViewHolder, position: Int) {
    holder.bind(contents[holder.adapterPosition])
  }

  override fun getItemCount(): Int = contents.size

  inner class RecommendationViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bind(content: String) {
      with(itemView) { tv_item_recommendation.text = content }
    }
  }
}