package id.kotlin.start.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import id.kotlin.start.R
import id.kotlin.start.model.NotificationModel
import kotlinx.android.synthetic.main.view_today.view.*
import kotlinx.android.synthetic.main.view_yesterday.view.*

class NotificationAdapter(private val model: NotificationModel) : Adapter<ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
    when (viewType) {
      0 -> TodayViewHolder(
        LayoutInflater.from(parent.context).inflate(
          R.layout.view_today,
          parent,
          false
        ), model.todays
      )
      1 -> YesterdayViewHolder(
        LayoutInflater.from(parent.context).inflate(
          R.layout.view_yesterday,
          parent,
          false
        ), model.yesterdays
      )
      else -> throw Exception("Unknown adapter!")
    }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    when (holder) {
      is TodayViewHolder -> holder.bind()
      is YesterdayViewHolder -> holder.bind()
    }
  }

  override fun getItemCount(): Int = 2

  override fun getItemViewType(position: Int): Int =
    when (position) {
      0 -> 0
      1 -> 1
      else -> throw Exception("Unknown adapter type!")
    }

  inner class TodayViewHolder(
    itemView: View,
    private val todays: List<String>
  ) : ViewHolder(itemView) {

    fun bind() {
      with(itemView) {
        rv_today.let { view ->
          view.addItemDecoration(
            DividerItemDecoration(
              view.context,
              DividerItemDecoration.VERTICAL
            )
          )
          view.adapter = TodayAdapter(todays)
        }
      }
    }
  }

  inner class YesterdayViewHolder(
    itemView: View,
    private val yesterdays: List<String>
  ) : ViewHolder(itemView) {

    fun bind() {
      with(itemView) {
        rv_yesterday.let { view ->
          view.addItemDecoration(
            DividerItemDecoration(
              view.context,
              DividerItemDecoration.VERTICAL
            )
          )
          view.adapter = YesterdayAdapter(yesterdays)
        }
      }
    }
  }
}