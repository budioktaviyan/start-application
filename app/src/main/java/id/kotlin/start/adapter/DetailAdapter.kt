package id.kotlin.start.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.tabs.TabLayout
import id.kotlin.start.fragment.HistoryFragment
import id.kotlin.start.fragment.NotificationFragment
import id.kotlin.start.fragment.StatusFragment

class DetailAdapter(
  fragmentManager: FragmentManager,
  private val layout: TabLayout,
  private val tabs: List<TabLayout.Tab>
) : FragmentStatePagerAdapter(fragmentManager) {

  init { tabs.map { layout.addTab(it) } }

  override fun getItem(position: Int): Fragment =
    when (position) {
      0 -> StatusFragment()
      1 -> HistoryFragment()
      2 -> NotificationFragment()
      else -> throw Exception("Unknown fragment!")
    }

  override fun getCount(): Int = tabs.size
}