package id.kotlin.start.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.kotlin.start.R
import id.kotlin.start.adapter.StatusAdapter
import id.kotlin.start.model.StatusModel
import kotlinx.android.synthetic.main.fragment_status.*

class StatusFragment : Fragment() {

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = LayoutInflater.from(context).inflate(R.layout.fragment_status, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val recommendations = listOf("Satu", "Dua", "Tiga", "Empat", "Lima")
    val products = listOf(
      "Produk Satu",
      "Produk Dua",
      "Produk Tiga",
      "Produk Empat",
      "Produk Lima",
      "Produk Enam",
      "Produk Tujuh",
      "Produk Delapan",
      "Produk Sembilan",
      "Produk Sepuluh"
    )
    rv_status.adapter = StatusAdapter(StatusModel(recommendations, products))
  }
}