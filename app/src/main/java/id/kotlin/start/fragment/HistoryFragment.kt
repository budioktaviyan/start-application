package id.kotlin.start.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.kotlin.start.R
import id.kotlin.start.adapter.HistoryAdapter
import id.kotlin.start.model.HistoryModel
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = LayoutInflater.from(context).inflate(R.layout.fragment_history, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    val histories = mutableListOf<HistoryModel>()

    for (model in 0 until 10) {
      histories.add(HistoryModel("O666$model", "Success"))
    }

    for (model in 0 until 15) {
      histories.add(HistoryModel("0111$model", "Failed"))
    }

    rv_history.adapter = HistoryAdapter(histories.map { it })
  }
}