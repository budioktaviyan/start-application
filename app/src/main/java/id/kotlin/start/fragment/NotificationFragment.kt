package id.kotlin.start.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.kotlin.start.R
import id.kotlin.start.adapter.NotificationAdapter
import id.kotlin.start.model.NotificationModel
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment() {

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = LayoutInflater.from(context).inflate(R.layout.fragment_notification, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val todays = mutableListOf<String>()
    for (content in 0 until 10) {
      todays.add("Success")
    }

    val yesterdays = mutableListOf<String>()
    for (content in 0 until 10) {
      yesterdays.add("Expired")
    }

    rv_notification.adapter = NotificationAdapter(
      NotificationModel(
        todays.map { it },
        yesterdays.map { it }
      )
    )
  }
}