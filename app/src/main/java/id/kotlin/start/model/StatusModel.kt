package id.kotlin.start.model

data class StatusModel(
  val recommendations: List<String>,
  val products: List<String>
)