package id.kotlin.start.model

data class HistoryModel(
  val title: String,
  val description: String
)