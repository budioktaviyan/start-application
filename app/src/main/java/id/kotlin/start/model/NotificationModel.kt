package id.kotlin.start.model

data class NotificationModel(
  val todays: List<String>,
  val yesterdays: List<String>
)